# VTTA.io Quick  Fix



## How to do it

**You need access to your foundry server files to do this**

- First make a backup of the files you are about to change. _If something breaks even further you can uninstall and reinstall VTTA.io modules to reset everthing._

- You will be replacing the current files with the files I provided here

## Replace current module files

### Fix for VTTA Tokens

- Navigate to `/foundrydata/Data/modules/vtta-tokens/src/modules/editor/` My specific file location is `/home/ec2-user/foundrydata/Data/modules/vtta-tokens/src/modules/editor/` but your server may be different. Just do a grep or find. 

- Replace `UI.js` with the `UI.js` provided here.

- Save and Refresh your foundry. Test to see if you can save and upload tokens

### Fix for VTTA.io DnD Beyond Importer

- Navigate to `/foundrydata/Data/modules/vtta-ddb/src/modules/extension/utilities/` My specific file location is `/home/ec2-user/foundrydata/Data/modules/vtta-ddb/src/modules/extension/utilities/` but your server may be differnt. Just do a grep or find

- Replace `folder.js` with the `folder.js` file provided. 

- Replace `world.js` with `world.js` file provided

- Save and refresh your foundry.
- Connect VTTA.io extension to your foundry server
- Open DnD Beyond and click import button (it should be showing now)

**NOTE**
I don't think this fixes importing items or spells but it works enough to import monsters. When it imports, it will create an empty folder of the creature type and then a black folder similiar but the action monster will be imported there. All you need to do is delete the empty folder and move the monster to the folder of choice, then delete that black folder as well. 

This is in no way a full fix for VTTA.io importer but it works enough to allow importing of full creature stat blocks into foundry. 


